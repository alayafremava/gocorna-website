import "./styles.css";
import { StaySafePage } from "./pages/StaySafePage/StaySafePage";
import { HealthcarePage } from "./pages/HealthcarePage/HealthcarePage";
import { HeroPage } from "./pages/HeroPage/HeroPage";
import { Header } from "./shared/Header/Header";
import { ExpertsPage } from "./pages/ExpertsPage/ExpertsPage";

const app = document.querySelector("#app"); // div#app

app.append(Header());
app.append(HeroPage());
app.append(StaySafePage());
app.append(ExpertsPage());
app.append(await HealthcarePage());
