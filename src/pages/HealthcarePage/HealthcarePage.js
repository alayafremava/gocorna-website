import "./styles.css";
import { getHealthcareCards } from "../../assigment/api/getHealthcareCards";
import { HealthcareCard } from "../../shared/HealthcareCard/HealthcareCard";
import { BGSquare } from "../../shared/BGSquare/BGSquare";
import { ColoredTitle } from "../../shared/ColoredTitle/ColoredTitle";

export async function HealthcarePage() {
  const page = document.createElement("section");
  page.className = "section-healcare";
  const cardsBlock = document.createElement("div");
  const storeBlock = document.createElement("div");
  const btnGooglePlay = document.createElement("button");
  const btnAppStore = document.createElement("button");
  const imgGooglePlay = document.createElement("img");
  const imgAppStore = document.createElement("img");
  const p = document.createElement("p");

  cardsBlock.className = "cards";
  const cards = await getHealthcareCards();
  cards.map((card) =>
    cardsBlock.append(HealthcareCard(card.title, card.text, card.iconLink))
  );

  p.className = "context";
  p.innerText =
    "Bringing premium healthcare features to your fingertips. User friendly mobile platform to \nbring healthcare to your fingertips. Signup and be a part of the new health culture.";

  storeBlock.className = "store";
  imgGooglePlay.src = "src/assets/icons/google_play.png";
  imgAppStore.src = "src/assets/icons/apple_store.png";

  btnGooglePlay.append(imgGooglePlay);
  btnAppStore.append(imgAppStore);
  storeBlock.append(btnGooglePlay);
  storeBlock.append(btnAppStore);

  page.append(ColoredTitle("red", " at your Fingertips.", "Healthcare", true));
  page.append(p);
  page.append(cardsBlock);
  page.append(storeBlock);
  page.append(BGSquare(65, 15, -30, 64, 0, 0, 5));
  page.append(BGSquare(97, 30, 105, 31, 0, 0, 27));
  page.append(BGSquare(127, 30, -30, 52, 0, 0, 58));
  page.append(BGSquare(65, 15, -30, 19, 0, 0, 90));

  return page;
}
