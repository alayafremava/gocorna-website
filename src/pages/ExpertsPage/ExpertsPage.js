import "./styles.css";
import { AppButton } from "../../shared/AppButton/AppButton";
import { ColoredTitle } from "../../shared/ColoredTitle/ColoredTitle";

export function ExpertsPage() {
  const page = document.createElement("section");
  page.className = "section__experts";

  const pageContainer = document.createElement("div");
  pageContainer.className = "page-container-expetrs";

  const mainContainer = document.createElement("div");
  mainContainer.className = "mainContainer";

  const imageContainer = document.createElement("div");
  imageContainer.className = "image-container";

  const image = document.createElement("img");
  image.src = "./src/assets/image/talk_to_expert.png";
  image.className = "experts-image";

  const contentContainer = document.createElement("div");
  contentContainer.className = "content-container experts-container";

  const heading = ColoredTitle("blue", "Talk to", " experts.");

  const text = document.createElement("p");
  text.textContent =
    "Book appointments or submit queries into thousands of forums concerning health issues and prevention against novel Coronavirus.";
  text.className = "custom-text";

  const button = AppButton("Features", "red");

  contentContainer.appendChild(heading);
  contentContainer.appendChild(text);
  contentContainer.appendChild(button);

  imageContainer.appendChild(image);

  mainContainer.appendChild(imageContainer);
  mainContainer.appendChild(contentContainer);

  const achivmentContainer = document.createElement("div");
  achivmentContainer.className = "achivment-container";

  const achivment1 = document.createElement("div");
  const achivmentList1 = document.createElement("ul");
  const achivmentItem1a = document.createElement("li");
  const achivmentItem1b = document.createElement("li");

  achivmentItem1a.textContent = "2m";
  achivmentItem1b.textContent = "Users";

  achivmentList1.className = "achivment-list";
  achivmentItem1a.className = "achivment-item";
  achivmentItem1b.className = "achivment-item";

  achivmentList1.appendChild(achivmentItem1a);
  achivmentList1.appendChild(achivmentItem1b);
  achivment1.appendChild(achivmentList1);

  const achivment2 = document.createElement("div");
  const achivmentList2 = document.createElement("ul");
  const achivmentItem2a = document.createElement("li");
  const achivmentItem2b = document.createElement("li");

  achivmentItem2a.textContent = "78";
  achivmentItem2b.textContent = "Countries";

  achivmentList2.className = "achivment-list";
  achivmentItem2a.className = "achivment-item";
  achivmentItem2b.className = "achivment-item";

  achivmentList2.appendChild(achivmentItem2a);
  achivmentList2.appendChild(achivmentItem2b);
  achivment2.appendChild(achivmentList2);

  const achivment3 = document.createElement("div");
  const achivmentList3 = document.createElement("ul");
  const achivmentItem3a = document.createElement("li");
  const achivmentItem3b = document.createElement("li");

  achivmentItem3a.textContent = "10,000+";
  achivmentItem3b.textContent = "Medical experts";

  achivmentList3.className = "achivment-list";
  achivmentItem3a.className = "achivment-item";
  achivmentItem3b.className = "achivment-item";

  achivmentList3.appendChild(achivmentItem3a);
  achivmentList3.appendChild(achivmentItem3b);
  achivment3.appendChild(achivmentList3);

  achivmentContainer.appendChild(achivment1);
  achivmentContainer.appendChild(achivment2);
  achivmentContainer.appendChild(achivment3);

  const backgroundImage = document.createElement("img");
  backgroundImage.src = "./src/assets/image/behin_statistic.png";
  backgroundImage.className = "background-image";

  const rectangleContainer = document.createElement("div");
  rectangleContainer.className = "rectangle-container";

  const achivmentWrapper = document.createElement("div");
  achivmentWrapper.className = "achivment-wrapper";

  achivmentWrapper.appendChild(backgroundImage);
  achivmentWrapper.appendChild(rectangleContainer);
  achivmentWrapper.appendChild(achivmentContainer);

  pageContainer.appendChild(achivmentWrapper);
  pageContainer.appendChild(mainContainer);

  page.append(pageContainer);

  return page;
}
