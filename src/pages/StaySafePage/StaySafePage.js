import "./styles.css";
import { AppButton } from "../../shared/AppButton/AppButton";
import { ColoredTitle } from "../../shared/ColoredTitle/ColoredTitle";

export function StaySafePage() {
  const page = document.createElement("section");
  page.className = "section__stay-safe";
  const pageContainer = document.createElement("div");
  pageContainer.className = "page-container";

  const imageContainer = document.createElement("div");
  imageContainer.className = "image-container";

  const image = document.createElement("img");
  image.src = "./src/assets/image/mobile_app.png";
  image.className = "custom-image";

  const contentContainer = document.createElement("div");
  contentContainer.className = "content-container";

  const heading = ColoredTitle("red", "Stay safe with Go", "Corona.");

  const text = document.createElement("p");
  text.textContent =
    "24x7 Support and user friendly mobile platform to bring healthcare to your fingertips. Signup and be a part of the new health culture.";
  text.className = "custom-text";

  const button = AppButton("Features", "red");

  contentContainer.appendChild(heading);
  contentContainer.appendChild(text);
  contentContainer.appendChild(button);

  imageContainer.appendChild(image);

  pageContainer.appendChild(imageContainer);
  pageContainer.appendChild(contentContainer);

  page.append(pageContainer);

  return page;
}
