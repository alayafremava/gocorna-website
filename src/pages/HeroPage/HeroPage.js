import { ColoredTitle } from "../../shared/ColoredTitle/ColoredTitle";
import { AppButton } from "../../shared/AppButton/AppButton";
import "./HeroPage.css";

export function HeroPage() {
  const sectionHero = document.createElement("section");
  sectionHero.className = "hero";

  const sectionHeroContainer = document.createElement("div");
  sectionHeroContainer.className = "section-hero-container";

  const sectionHeroContent = document.createElement("div");
  sectionHeroContent.className = "section-hero-content";

  const textDiv = document.createElement("div");
  textDiv.className = "text";

  const paragraph = document.createElement("p");
  paragraph.textContent =
    "All in one destination for COVID-19 health queries. Consult 10,000+ health workers about your concerns.";

  textDiv.append(paragraph);

  const imageDoctorDiv = document.createElement("div");
  imageDoctorDiv.className = "image-doctor";

  const imgHero = document.createElement("img");
  imgHero.className = "img-hero";
  imgHero.src = "src/assets/image/hero_img.png";
  imgHero.alt = "doctor";

  imageDoctorDiv.append(imgHero);

  const redRectangle = document.createElement("div");
  redRectangle.className = "red-rectangle";

  const playGocornaDiv = document.createElement("div");
  playGocornaDiv.className = "play-gocorna";

  const playIcon = document.createElement("img");
  playIcon.className = "play";
  playIcon.src = "src/assets/icons/play.svg";
  playIcon.alt = "play";

  const textGocornaDiv = document.createElement("div");
  textGocornaDiv.className = "text-gocorna";

  const playText = document.createElement("p");
  playText.textContent = "Stay safe with GoCorona";

  const watchVideoLink = document.createElement("a");
  watchVideoLink.className = "watch-video";
  watchVideoLink.href = "#";
  watchVideoLink.textContent = "Watch Video";

  playGocornaDiv.append(playIcon);
  playGocornaDiv.append(textGocornaDiv);
  textGocornaDiv.append(playText);
  textGocornaDiv.append(watchVideoLink);

  sectionHeroContent.append(
    ColoredTitle("blue", "Take care of yours family’s ", "health.", false, true)
  );
  sectionHeroContent.append(textDiv);
  sectionHeroContent.append(AppButton("Get started"));
  sectionHeroContent.append(playGocornaDiv);
  sectionHeroContainer.append(imageDoctorDiv);

  sectionHeroContainer.append(sectionHeroContent);

  sectionHero.append(sectionHeroContainer);
  sectionHero.append(redRectangle);

  return sectionHero;
}
