import "./styles.css";

export function HealthcareCard(title, text, iconLink) {
  const card = document.createElement("div");
  card.className = "card";

  const img = document.createElement("img");
  img.className = "card__icon";
  img.src = iconLink;

  const h3 = document.createElement("h3");
  h3.className = "card__title";
  h3.innerText = title;

  const context = document.createElement("p");
  context.className = "card__text";
  context.innerText = text;

  card.append(img);
  card.append(h3);
  card.append(context);

  return card;
}
