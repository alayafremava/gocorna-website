import "./styles.css";

export function BGSquare(
  height = 50,
  borderRadius = 15,
  deg = 0,
  top,
  right,
  bottom,
  left
) {
  const div = document.createElement("div");

  div.className = "square";
  div.style.width = height + "px";
  div.style.height = height + "px";
  div.style.transform = "rotate(" + deg + "deg)";
  div.style.top = top + "%";
  div.style.right = right + "%";
  div.style.bottom = bottom + "%";
  div.style.left = left + "%";
  div.style.borderRadius = borderRadius + "px";

  return div;
}
