import { AppButton } from "../AppButton/AppButton";
import "./header.css";
export function Header() {
  const header = document.createElement("header");
  const headerContainer = document.createElement("div");
  header.classList.add("header");
  headerContainer.classList.add("header-container");

  const logo = document.createElement("div");
  logo.classList.add("logo");

  const logoImage = document.createElement("img");
  logoImage.classList.add("logo-img");
  logoImage.src = "./src/assets/icons/logo.png";
  logoImage.alt = "logo";

  const navigation = document.createElement("nav");
  navigation.classList.add("header-navigation");

  const ul = document.createElement("ul");
  ul.classList.add("header-list");

  const menuItems = ["HOME", "FEATURES", "SUPPORT", "CONTACT US"];

  menuItems.forEach((itemText) => {
    const li = document.createElement("li");
    li.classList.add("header-item");

    const a = document.createElement("a");
    a.classList.add("header-link");
    a.href = "#";
    a.textContent = itemText;

    li.appendChild(a);
    ul.appendChild(li);
  });

  logo.append(logoImage);
  headerContainer.append(logo);
  headerContainer.append(navigation);
  headerContainer.append(AppButton("Download", "blue"));
  header.append(headerContainer);
  navigation.append(ul);

  return header;
}
