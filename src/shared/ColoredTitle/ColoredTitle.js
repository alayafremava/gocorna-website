import "./styles.css";

export function ColoredTitle(
  color,
  text,
  coloredText,
  isColoredTextFirst,
  isHeroSection = false
) {
  const headingTag = isHeroSection ? "h1" : "h2";
  const heading = document.createElement(headingTag);
  heading.classList.add("section__title");

  const coloredTextSpan = document.createElement("span");
  coloredTextSpan.className =
    color === "red" ? "section__title--red" : "section__title--blue";
  coloredTextSpan.textContent = coloredText;

  if (isColoredTextFirst) {
    heading.append(coloredTextSpan, text);
  } else {
    heading.append(text, coloredTextSpan);
  }

  return heading;
}
