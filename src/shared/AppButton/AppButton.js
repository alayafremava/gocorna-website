import "./styles.css";

export function AppButton(label = "", color = "red") {
  const button = document.createElement("button");
  button.className = color === "red" ? "red-button" : "blue-button";
  button.classList.add("app-btn");

  button.innerText = label;

  return button;
}
