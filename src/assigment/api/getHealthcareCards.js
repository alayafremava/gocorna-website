import cards from "../db/healthcareCardsDB.json";

export function getHealthcareCards() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve([...cards]);
    }, 500);
  });
}
